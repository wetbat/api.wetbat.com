import { ApiProperty } from '@nestjs/swagger';

export class ContactDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  email: string;
}
