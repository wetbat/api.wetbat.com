import { ApiProperty } from '@nestjs/swagger';
import { DepartureDto } from '@swagger/departure.dto';
import { ArrivalDto } from '@swagger/arrival.dto';
import { ContactDto } from '@swagger/contact.dto';

export class ServiceDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  price: number;

  @ApiProperty()
  orderedAt: Date;

  @ApiProperty({ type: DepartureDto })
  departure: DepartureDto;

  @ApiProperty({ type: ArrivalDto })
  arrival: ArrivalDto;

  @ApiProperty()
  travalers: number;

  @ApiProperty()
  transportation: string;

  @ApiProperty({ type: ContactDto })
  contact: ContactDto;
}
