import { ApiProperty } from '@nestjs/swagger';
import { QuoteDto } from '@swagger/quote.dto';

export class FindAllQuotesDto {
  @ApiProperty({ type: [QuoteDto] })
  quotes: QuoteDto[];
}
