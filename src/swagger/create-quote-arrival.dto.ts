import { ApiProperty } from '@nestjs/swagger';

export class CreateQuoteArrivalDto {
  @ApiProperty()
  airportId: string;
}
