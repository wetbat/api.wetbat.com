import { ApiProperty } from '@nestjs/swagger';

export class QuotePriceDto {
  @ApiProperty({ example: '3.21' })
  price: number;
}
