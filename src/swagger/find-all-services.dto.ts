import { ApiProperty } from '@nestjs/swagger';
import { ServiceDto } from '@swagger/service.dto';

export class FindAllServicesDto {
  @ApiProperty({ type: [ServiceDto] })
  services: ServiceDto[];
}
