import { ApiProperty } from '@nestjs/swagger';

export class CreateAirportDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  street: string;

  @ApiProperty()
  city: string;

  @ApiProperty()
  state: string;

  @ApiProperty()
  country: string;

  @ApiProperty()
  latitude: number;

  @ApiProperty()
  longitude: number;
}
