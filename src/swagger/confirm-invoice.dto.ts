import { Invoice } from '@invoices/domain/invoice.entity';
import { ApiProperty } from '@nestjs/swagger';

export class ConfirmInvoiceDto {
  @ApiProperty({
    type: 'enum',
    enum: Invoice.Status,
    example: Invoice.Status.PAID,
  })
  status: Invoice.Status;

  @ApiProperty({ example: 'b8392cbb-c36a-4ff8-838e-2dd49313a595' })
  serviceId: string;

  @ApiProperty({ example: 'b8392cbb-c36a-4ff8-838e-2dd49313a595' })
  id: string;

  @ApiProperty()
  statusChangedAt: Date;
}
