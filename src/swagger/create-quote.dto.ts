import { CreateQuoteDepartureDto } from '@swagger/create-quote-departure.dto';
import { CreateQuoteArrivalDto } from '@swagger/create-quote-arrival.dto';
import { CreateQuoteContactDto } from '@swagger/create-quote-contact.dto';

import { ApiProperty } from '@nestjs/swagger';

export class CreateQuoteDto {
  @ApiProperty({ type: CreateQuoteDepartureDto })
  departure: CreateQuoteDepartureDto;

  @ApiProperty({ type: CreateQuoteArrivalDto })
  arrival: CreateQuoteArrivalDto;

  @ApiProperty()
  travalers: number;

  @ApiProperty()
  transportation: string;

  @ApiProperty({ type: CreateQuoteContactDto })
  contact: CreateQuoteContactDto;
}
