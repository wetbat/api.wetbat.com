import { ApiProperty } from '@nestjs/swagger';

export class CreateQuoteDepartureDto {
  @ApiProperty()
  date: Date;

  @ApiProperty()
  airportId: string;
}
