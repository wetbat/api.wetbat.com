import { Invoice } from '@invoices/domain/invoice.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ServiceDto } from '@swagger/service.dto';

export class InvoiceDto {
  @ApiProperty({ type: 'enum', enum: Invoice.Status })
  status: Invoice.Status;

  @ApiProperty()
  serviceId: string;

  @ApiProperty()
  id: string;

  @ApiProperty()
  statusChangedAt: Date;

  @ApiProperty({ type: ServiceDto })
  service: ServiceDto;
}
