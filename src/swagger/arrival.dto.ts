import { ApiProperty } from '@nestjs/swagger';
import { AirportDto } from '@swagger/airport.dto';

export class ArrivalDto {
  @ApiProperty()
  date: Date;

  @ApiProperty({ type: AirportDto })
  airport: AirportDto;
}
