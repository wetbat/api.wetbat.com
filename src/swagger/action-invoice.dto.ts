import { ApiProperty } from '@nestjs/swagger';

export class ActionInvoiceDto {
  @ApiProperty({ example: 'b8392cbb-c36a-4ff8-838e-2dd49313a595' })
  id: string;
}
