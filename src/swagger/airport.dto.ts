import { ApiProperty } from '@nestjs/swagger';
import { AddressDto } from '@swagger/address.dto';
import { LocationDto } from '@swagger/location.dto';

export class AirportDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty({ type: AddressDto })
  address: AddressDto;

  @ApiProperty({ type: LocationDto })
  location: LocationDto;
}
