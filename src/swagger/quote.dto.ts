import { DepartureDto } from '@swagger/departure.dto';
import { ArrivalDto } from '@swagger/arrival.dto';
import { ContactDto } from '@swagger/contact.dto';

import { ApiProperty } from '@nestjs/swagger';

export class QuoteDto {
  @ApiProperty()
  id: string;

  @ApiProperty({ type: DepartureDto })
  departure: DepartureDto;

  @ApiProperty({ type: ArrivalDto })
  arrival: ArrivalDto;

  @ApiProperty()
  travalers: number;

  @ApiProperty()
  transportation: string;

  @ApiProperty({ type: ContactDto })
  contact: ContactDto;
}
