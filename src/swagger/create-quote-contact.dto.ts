import { ApiProperty } from '@nestjs/swagger';

export class CreateQuoteContactDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  email: string;
}
