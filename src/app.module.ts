import { QuotesModule } from './quotes/main/quotes.module';
import { AirportsModule } from './airports/main/airports.module';
import { ConfigModule } from '@nestjs/config';

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AddressSchema } from '@airports/infra/schemas/address.schema';
import { LocationSchema } from '@airports/infra/schemas/location.schema';
import { AirportSchema } from '@airports/infra/schemas/airport.schema';
import {
  ArrivalSchema,
  ContactSchema,
  DepartureSchema,
  QuoteSchema,
} from '@quotes/infra/schemas';
import { ServicesModule } from '@services/main/services.module';
import { ServiceSchema } from '@services/infra/schemas/service.schema';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { InvoicesModule } from '@invoices/main/invoices.module';
import { InvoiceSchema } from '@invoices/infra/schemas';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: ['.env'] }),
    QuotesModule,
    AirportsModule,
    ServicesModule,
    InvoicesModule,
    EventEmitterModule.forRoot(),
    TypeOrmModule.forRoot({
      type: process.env.DATABASE_TYPE as unknown as any,
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: [
        AddressSchema,
        LocationSchema,
        AirportSchema,
        ArrivalSchema,
        ContactSchema,
        DepartureSchema,
        QuoteSchema,
        ServiceSchema,
        InvoiceSchema,
      ],
      synchronize: true,
    }),
  ],
})
export class AppModule {}
