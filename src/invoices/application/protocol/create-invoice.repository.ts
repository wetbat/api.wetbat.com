import { Invoice } from '@invoices/domain/invoice.entity';
import { InvoiceRepository } from '@invoices/domain/invoice.repository';

export interface CreateInvoiceRepository extends InvoiceRepository {
  create(invoice: Invoice): Promise<void>;
}
