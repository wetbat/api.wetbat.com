import { Invoice } from '@invoices/domain/invoice.entity';
import { InvoiceRepository } from '@invoices/domain/invoice.repository';

export interface FindAllInvoicesRepository extends InvoiceRepository {
  findAll(): Promise<Invoice[]>;
}
