import { Invoice } from '@invoices/domain/invoice.entity';
import { InvoiceRepository } from '@invoices/domain/invoice.repository';

export interface FindInvoiceByIdRepository extends InvoiceRepository {
  findById(id: string): Promise<Invoice>;
}
