import { Invoice } from '@invoices/domain/invoice.entity';
import { InvoiceRepository } from '@invoices/domain/invoice.repository';

export interface UpdateInvoiceRepository extends InvoiceRepository {
  update(invoice: Invoice): Promise<void>;
}
