import { Inject, Injectable } from '@nestjs/common';
import { FindAllInvoices } from '@invoices/domain/find-all-invoices.usecase.protocol';
import { FindServiceByIdRepository } from '@services/application/protocols/find-service-by-id.repository.protocol';
import { FindAllInvoicesRepository } from '../protocol/find-all-invoices.repository';

@Injectable()
export class FindAllInvoicesUseCase implements FindAllInvoices {
  constructor(
    @Inject('InvoiceRepository')
    private readonly invoiceRepository: FindAllInvoicesRepository,
    @Inject('ServiceRepository')
    private readonly serviceRepository: FindServiceByIdRepository,
  ) {}

  async exec(): Promise<FindAllInvoices.OutputDto[]> {
    const invoices = await this.invoiceRepository.findAll();
    const invoiceWithService: Array<FindAllInvoices.OutputDto> = [];
    for (const invoice of invoices) {
      const service = await this.serviceRepository.findById(
        invoice.getServiceId(),
      );
      invoiceWithService.push({
        id: invoice.getId(),
        serviceId: invoice.getServiceId(),
        status: invoice.getStatus(),
        statusChangedAt: invoice.getStatusChangedAt(),
        service: {
          price: service.getPrice(),
          orderedAt: service.getOrderedAt(),
          arrival: {
            date: service.getArrival().date,
            airport: {
              name: service.getArrival().airport.getName(),
              address: {
                city: service.getArrival().airport.getAddress().city,
                country: service.getArrival().airport.getAddress().country,
                state: service.getArrival().airport.getAddress().state,
                street: service.getArrival().airport.getAddress().street,
              },
              location: {
                latitude: service.getArrival().airport.getLocation().latitude,
                longitude: service.getArrival().airport.getLocation().longitude,
              },
            },
          },
          departure: {
            date: service.getDeparture().date,
            airport: {
              name: service.getDeparture().airport.getName(),
              address: {
                city: service.getDeparture().airport.getAddress().city,
                country: service.getDeparture().airport.getAddress().country,
                state: service.getDeparture().airport.getAddress().state,
                street: service.getDeparture().airport.getAddress().street,
              },
              location: {
                latitude: service.getDeparture().airport.getLocation().latitude,
                longitude: service.getDeparture().airport.getLocation()
                  .longitude,
              },
            },
          },
          contact: {
            email: service.getContact().email,
            name: service.getContact().name,
            phone: service.getContact().phone,
          },
          id: service.getId(),
          transportation: service.getTransportation(),
          travalers: service.getTravelers(),
        },
      });
    }
    return invoiceWithService;
  }
}
