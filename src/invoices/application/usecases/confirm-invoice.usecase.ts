import { Inject, Injectable } from '@nestjs/common';
import { UpdateInvoiceRepository } from '@invoices/application/protocol/update-invoice.repository';
import { FindInvoiceByIdRepository } from '@invoices/application/protocol/find-invoice-by-id.repository';
import { ConfirmInvoice } from '@invoices/domain/confirm-invoice.usecase.protocol';

@Injectable()
export class ConfirmInvoiceUseCase implements ConfirmInvoice {
  constructor(
    @Inject('InvoiceRepository')
    private readonly invoiceRepository: UpdateInvoiceRepository &
      FindInvoiceByIdRepository,
  ) {}

  async exec(
    input: ConfirmInvoice.InputDto,
  ): Promise<ConfirmInvoice.OutputDto> {
    const invoice = await this.invoiceRepository.findById(input.id);
    invoice.confirmPayment();
    await this.invoiceRepository.update(invoice);
    return {
      id: invoice.getId(),
      serviceId: invoice.getServiceId(),
      status: invoice.getStatus(),
      statusChangedAt: invoice.getStatusChangedAt(),
    };
  }
}
