import { CancelInvoice } from '@invoices/domain/cancel-invoice.usecase.protocol';
import { Inject, Injectable } from '@nestjs/common';
import { UpdateInvoiceRepository } from '@invoices/application/protocol/update-invoice.repository';
import { FindInvoiceByIdRepository } from '@invoices/application/protocol/find-invoice-by-id.repository';

@Injectable()
export class CancelInvoiceUseCase implements CancelInvoice {
  constructor(
    @Inject('InvoiceRepository')
    private readonly invoiceRepository: UpdateInvoiceRepository &
      FindInvoiceByIdRepository,
  ) {}

  async exec(input: CancelInvoice.InputDto): Promise<CancelInvoice.OutputDto> {
    const invoice = await this.invoiceRepository.findById(input.id);
    invoice.cancelPayment();
    await this.invoiceRepository.update(invoice);
    return {
      id: invoice.getId(),
      serviceId: invoice.getServiceId(),
      status: invoice.getStatus(),
      statusChangedAt: invoice.getStatusChangedAt(),
    };
  }
}
