import { Invoice } from '@invoices/domain/invoice.entity';
import { QuoteConvertedIntoService } from '@invoices/domain/quote-converted-into-service.listener.protocol';
import { Inject, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { QuoteConvertedIntoServiceEvent } from '@services/domain/quote-converted-into-service.event';
import { CreateInvoiceRepository } from '@invoices/application/protocol/create-invoice.repository';

@Injectable()
export class QuoteConvertedIntoServiceListener
  implements QuoteConvertedIntoService
{
  constructor(
    @Inject('InvoiceRepository')
    private readonly invoiceRepository: CreateInvoiceRepository,
  ) {}

  @OnEvent('quote.converted')
  async handle(event: QuoteConvertedIntoServiceEvent): Promise<void> {
    const invoice = new Invoice(event.serviceId, Invoice.Status.PENDING);
    await this.invoiceRepository.create(invoice);
  }
}
