import { Entity } from '@core/entity';

export class Invoice extends Entity {
  private statusChangedAt: Date;

  constructor(
    private readonly serviceId: string,
    private status: Invoice.Status,
    id?: string,
  ) {
    super(id);
    if (status === Invoice.Status.PENDING) {
      this.statusChangedAt = new Date();
    }
  }

  public getServiceId(): string {
    return this.serviceId;
  }

  public getStatusChangedAt(): Date {
    return this.statusChangedAt;
  }

  public getStatus(): Invoice.Status {
    return this.status;
  }

  public setStatusChangedAt(statusChangedAt: Date): void {
    this.statusChangedAt = statusChangedAt;
  }

  confirmPayment(): void {
    this.status = Invoice.Status.PAID;
    this.statusChangedAt = new Date();
  }

  cancelPayment(): void {
    this.status = Invoice.Status.CANCELED;
    this.statusChangedAt = new Date();
  }

  public static create(
    serviceId: string,
    status: Invoice.Status,
    statusChangedAt: Date,
    id: string,
  ): Invoice {
    const invoice = new Invoice(serviceId, status, id);
    invoice.setStatusChangedAt(statusChangedAt);
    return invoice;
  }
}

export namespace Invoice {
  export enum Status {
    PENDING = 'PENDING',
    PAID = 'PAID',
    CANCELED = 'CANCELED',
  }
}
