import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';
import { Invoice } from '@invoices/domain/invoice.entity';

export interface FindAllInvoices
  extends UseCase<undefined, FindAllInvoices.OutputDto[]> {}

export namespace FindAllInvoices {
  export class OutputDto extends Dto {
    status: Invoice.Status;
    serviceId: string;
    id: string;
    statusChangedAt: Date;
    service: {
      id: string;
      price: number;
      orderedAt: Date;
      departure: {
        date: Date;
        airport: {
          name: string;
          address: {
            street: string;
            city: string;
            state: string;
            country: string;
          };
          location: {
            latitude: number;
            longitude: number;
          };
        };
      };
      arrival: {
        date: Date;
        airport: {
          name: string;
          address: {
            street: string;
            city: string;
            state: string;
            country: string;
          };
          location: {
            latitude: number;
            longitude: number;
          };
        };
      };
      travalers: number;
      transportation: string;
      contact: {
        name: string;
        phone: string;
        email: string;
      };
    };
  }
}
