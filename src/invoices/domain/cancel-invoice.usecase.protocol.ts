import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';
import { Invoice } from '@invoices/domain/invoice.entity';

export interface CancelInvoice
  extends UseCase<CancelInvoice.InputDto, CancelInvoice.OutputDto> {}

export namespace CancelInvoice {
  export class InputDto extends Dto {
    id: string;
  }
  export class OutputDto extends Dto {
    status: Invoice.Status;
    serviceId: string;
    id: string;
    statusChangedAt: Date;
  }
}
