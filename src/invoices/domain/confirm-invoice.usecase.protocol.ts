import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';
import { Invoice } from '@invoices/domain/invoice.entity';

export interface ConfirmInvoice
  extends UseCase<ConfirmInvoice.InputDto, ConfirmInvoice.OutputDto> {}

export namespace ConfirmInvoice {
  export class InputDto extends Dto {
    id: string;
  }
  export class OutputDto extends Dto {
    status: Invoice.Status;
    serviceId: string;
    id: string;
    statusChangedAt: Date;
  }
}
