import { Listener } from '@core/listener';
import { QuoteConvertedIntoServiceEvent } from '@services/domain/quote-converted-into-service.event';

export interface QuoteConvertedIntoService
  extends Listener<QuoteConvertedIntoServiceEvent> {
  handle(event: QuoteConvertedIntoServiceEvent): Promise<void>;
}
