import { Repository } from '@core/repository';
import { Invoice } from '@invoices/domain/invoice.entity';

export interface InvoiceRepository extends Repository<Invoice> {}
