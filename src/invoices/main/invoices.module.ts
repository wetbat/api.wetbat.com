import { Module } from '@nestjs/common';
import { QuoteConvertedIntoServiceListener } from '@invoices/application/listeners/quote-converted-into-service.listener';
import { InvoiceMySqlRepositoryModule } from '@invoices/main/invoice-mysql.repository.module';
import { CancelInvoiceController } from '@invoices/presentation/cancel-invoice.controller';
import { CancelInvoiceUseCase } from '@invoices/application/usecases/cancel-invoice.usecase';
import { ConfirmInvoiceController } from '@invoices/presentation/confirm-invoice.controller';
import { ConfirmInvoiceUseCase } from '@invoices/application/usecases/confirm-invoice.usecase';
import { FindAllInvoicesController } from '@invoices/presentation/find-all-invoices.controller';
import { FindAllInvoicesUseCase } from '@invoices/application/usecases/find-all-invoices.usecase';
import { ServiceMySqlRepositoryModule } from '@services/main/service-mysql.repository.module';

@Module({
  controllers: [
    CancelInvoiceController,
    ConfirmInvoiceController,
    FindAllInvoicesController,
  ],
  providers: [
    QuoteConvertedIntoServiceListener,
    CancelInvoiceUseCase,
    ConfirmInvoiceUseCase,
    FindAllInvoicesUseCase,
  ],
  imports: [InvoiceMySqlRepositoryModule, ServiceMySqlRepositoryModule],
  exports: [QuoteConvertedIntoServiceListener],
})
export class InvoicesModule {}
