import { InvoiceMysqlRepository } from '@invoices/infra/invoice-mysq.repository';
import { InvoiceSchema } from '@invoices/infra/schemas';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  providers: [
    {
      provide: 'InvoiceRepository',
      useClass: InvoiceMysqlRepository,
    },
  ],
  exports: [TypeOrmModule.forFeature([InvoiceSchema]), 'InvoiceRepository'],
  imports: [TypeOrmModule.forFeature([InvoiceSchema])],
})
export class InvoiceMySqlRepositoryModule {}
