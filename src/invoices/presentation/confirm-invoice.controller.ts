import { ConfirmInvoiceUseCase } from '@invoices/application/usecases/confirm-invoice.usecase';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags, ApiCreatedResponse } from '@nestjs/swagger';
import { ActionInvoiceDto } from '@swagger/action-invoice.dto';
import { ConfirmInvoiceDto } from '@swagger/confirm-invoice.dto';

@ApiTags('invoices')
@Controller('invoices')
export class ConfirmInvoiceController {
  constructor(private readonly confirmInvoiceUseCase: ConfirmInvoiceUseCase) {}

  @Post('confirm')
  @ApiCreatedResponse({
    type: ConfirmInvoiceDto,
    description: 'The record was confirmed.',
  })
  confirm(@Body() confirmDto: ActionInvoiceDto): Promise<ConfirmInvoiceDto> {
    return this.confirmInvoiceUseCase.exec({ id: confirmDto.id });
  }
}
