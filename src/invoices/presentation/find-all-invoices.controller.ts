import { FindAllInvoicesUseCase } from '@invoices/application/usecases/find-all-invoices.usecase';
import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { InvoiceDto } from '@swagger/invoice.dto';

@ApiTags('invoices')
@Controller('invoices')
export class FindAllInvoicesController {
  constructor(
    private readonly findAllInvoicesUseCase: FindAllInvoicesUseCase,
  ) {}

  @Get()
  @ApiOkResponse({ type: [InvoiceDto] })
  findAll() {
    return this.findAllInvoicesUseCase.exec();
  }
}
