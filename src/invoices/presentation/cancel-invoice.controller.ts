import { CancelInvoiceUseCase } from '@invoices/application/usecases/cancel-invoice.usecase';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags, ApiCreatedResponse } from '@nestjs/swagger';
import { ActionInvoiceDto } from '@swagger/action-invoice.dto';
import { CancelInvoiceDto } from '@swagger/cancel-invoice.dto';

@ApiTags('invoices')
@Controller('invoices')
export class CancelInvoiceController {
  constructor(private readonly cancelInvoiceUseCase: CancelInvoiceUseCase) {}

  @Post('cancel')
  @ApiCreatedResponse({
    type: CancelInvoiceDto,
    description: 'The record was canceled.',
  })
  cancel(@Body() cancelDto: ActionInvoiceDto): Promise<CancelInvoiceDto> {
    return this.cancelInvoiceUseCase.exec({ id: cancelDto.id });
  }
}
