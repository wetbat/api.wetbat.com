import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateInvoiceRepository } from '@invoices/application/protocol/create-invoice.repository';
import { InvoiceSchema } from '@invoices/infra/schemas';
import { Invoice } from '@invoices/domain/invoice.entity';
import { FindInvoiceByIdRepository } from '@invoices/application/protocol/find-invoice-by-id.repository';
import { UpdateInvoiceRepository } from '@invoices/application/protocol/update-invoice.repository';
import { FindAllInvoicesRepository } from '@invoices/application/protocol/find-all-invoices.repository';

export class InvoiceMysqlRepository
  implements
    CreateInvoiceRepository,
    FindInvoiceByIdRepository,
    UpdateInvoiceRepository,
    FindAllInvoicesRepository
{
  constructor(
    @InjectRepository(InvoiceSchema)
    private readonly invoiceSchemaRepository: Repository<InvoiceSchema>,
  ) {}

  async create(invoice: Invoice): Promise<void> {
    const invoiceSchema = InvoiceSchema.map(invoice);
    await this.invoiceSchemaRepository.insert(invoiceSchema);
  }

  async findById(id: string): Promise<Invoice> {
    const invoiceSchema = await this.invoiceSchemaRepository.findOneOrFail({
      where: { id },
      relations: { service: true },
    });
    const invoice = Invoice.create(
      invoiceSchema.serviceId,
      invoiceSchema.status,
      invoiceSchema.statusChangedAt,
      invoiceSchema.id,
    );
    return invoice;
  }

  async update(invoice: Invoice): Promise<void> {
    const invoiceSchema = InvoiceSchema.create(
      invoice.getId(),
      invoice.getServiceId(),
      invoice.getStatus(),
      invoice.getStatusChangedAt(),
    );
    await this.invoiceSchemaRepository.update(
      { id: invoice.getId() },
      invoiceSchema,
    );
  }

  async findAll(): Promise<Invoice[]> {
    const invoicesSchema = await this.invoiceSchemaRepository.find();
    return invoicesSchema.map((invoiceSchema) => {
      const invoice = Invoice.create(
        invoiceSchema.serviceId,
        invoiceSchema.status,
        invoiceSchema.statusChangedAt,
        invoiceSchema.id,
      );
      return invoice;
    });
  }
}
