import { Entity, Column, PrimaryColumn, OneToOne, JoinColumn } from 'typeorm';
import { Invoice } from '@invoices/domain/invoice.entity';
import { ServiceSchema } from '@services/infra/schemas/service.schema';

@Entity({ name: 'invoices' })
export class InvoiceSchema {
  @PrimaryColumn('uuid')
  id: string;

  @Column()
  serviceId: string;

  @Column({ type: 'enum', enum: Invoice.Status })
  status: Invoice.Status;

  @Column()
  statusChangedAt: Date;

  @OneToOne(() => ServiceSchema)
  @JoinColumn({ name: 'serviceId' })
  service: ServiceSchema;

  public static map(invoice: Invoice): InvoiceSchema {
    const invoiceSchema = new InvoiceSchema();
    invoiceSchema.id = invoice.getId();
    invoiceSchema.serviceId = invoice.getServiceId();
    invoiceSchema.status = invoice.getStatus();
    invoiceSchema.statusChangedAt = invoice.getStatusChangedAt();
    return invoiceSchema;
  }

  public static create(
    id: string,
    serviceId: string,
    status: Invoice.Status,
    statusChangedAt: Date,
  ): InvoiceSchema {
    const invoiceSchema = new InvoiceSchema();
    invoiceSchema.id = id;
    invoiceSchema.serviceId = serviceId;
    invoiceSchema.status = status;
    invoiceSchema.statusChangedAt = statusChangedAt;
    return invoiceSchema;
  }
}
