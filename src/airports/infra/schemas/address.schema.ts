import { Address } from '@airports/domain/address.valueobject';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'addresses' })
export class AddressSchema {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  street: string;

  @Column()
  city: string;

  @Column()
  state: string;

  @Column()
  country: string;

  public static map(address: Address): AddressSchema {
    const addressSchema = new AddressSchema();
    addressSchema.city = address.city;
    addressSchema.country = address.country;
    addressSchema.state = address.state;
    addressSchema.street = address.street;
    return addressSchema;
  }
}
