import { Location } from '@airports/domain/location.valueobject';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'locations' })
export class LocationSchema {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ precision: 11, scale: 9, type: 'decimal' })
  latitude: number;

  @Column({ precision: 12, scale: 9, type: 'decimal' })
  longitude: number;

  public static create(latitude: number, longitude: number): LocationSchema {
    const locationSchema = new LocationSchema();
    locationSchema.latitude = latitude;
    locationSchema.longitude = longitude;
    return locationSchema;
  }

  public static map(location: Location): LocationSchema {
    const locationSchema = new LocationSchema();
    locationSchema.latitude = location.latitude;
    locationSchema.longitude = location.longitude;
    return locationSchema;
  }
}
