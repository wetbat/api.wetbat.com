import {
  Entity,
  Column,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { AddressSchema } from './address.schema';
import { Airport } from '@airports/domain/airport.entity';
import { LocationSchema } from './location.schema';
import {
  ArrivalSchema,
  DepartureSchema,
  QuoteSchema,
} from '@quotes/infra/schemas';

@Entity({ name: 'airports' })
export class AirportSchema {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @OneToOne(() => AddressSchema)
  @JoinColumn()
  address: AddressSchema;

  @OneToOne(() => LocationSchema)
  @JoinColumn()
  location: LocationSchema;

  @OneToMany(() => ArrivalSchema, (arrival) => arrival.airport)
  arrivals: ArrivalSchema[];

  @OneToMany(() => DepartureSchema, (departure) => departure.airport)
  departures: DepartureSchema[];

  public static map(airport: Airport): AirportSchema {
    const airportSchema = new AirportSchema();
    airportSchema.name = airport.getName();
    airportSchema.address = AddressSchema.map(airport.getAddress());
    airportSchema.location = LocationSchema.map(airport.getLocation());
    airportSchema.id = airport.getId();
    return airportSchema;
  }
}
