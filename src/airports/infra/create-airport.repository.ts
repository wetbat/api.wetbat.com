import { Repository } from '@core/repository';
import { Airport } from '@airports/domain/airport.entity';

export interface CreateAirportRepository extends Repository<Airport> {
  create(airport: Airport): Promise<void>;
}
