import { AddressSchema } from '../infra/schemas/address.schema';

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AirportMySqlRepository } from '../domain/airport-mysql.repository';
import { LocationSchema } from '../infra/schemas/location.schema';
import { AirportSchema } from '../infra/schemas/airport.schema';

@Module({
  providers: [
    {
      provide: 'AirportRepository',
      useClass: AirportMySqlRepository,
    },
  ],
  exports: [
    TypeOrmModule.forFeature([AddressSchema, LocationSchema, AirportSchema]),
    'AirportRepository',
  ],
  imports: [
    TypeOrmModule.forFeature([AddressSchema, LocationSchema, AirportSchema]),
  ],
})
export class AirportRepositoryModule {}
