import { CreateAirportUseCase } from '@airports/application/usecases/create-airport.usecase';
import { CreateAirportsController } from '@airports/presentation/create-airport.controller';
import { AirportMySqlRepository } from '@airports/domain/airport-mysql.repository';
import { FindAllAirportsUseCase } from '@airports/application/usecases/find-all-airports.usecase';
import { AirportRepositoryModule } from '@airports/main/airport.repository.module';
import { FindAirportByIdController } from '@airports/presentation/find-airport-by-id.controller';
import { FindAirportByIdUseCase } from '@airports/application/usecases/find-airport-by-id.usecase';
import { FindAllAirportsController } from '@airports/presentation/find-all-airports.controller';

import { Module } from '@nestjs/common';

@Module({
  controllers: [
    CreateAirportsController,
    FindAirportByIdController,
    FindAllAirportsController,
  ],
  providers: [
    CreateAirportUseCase,
    AirportMySqlRepository,
    FindAirportByIdUseCase,
    FindAllAirportsUseCase,
  ],
  imports: [AirportRepositoryModule, AirportsModule],
})
export class AirportsModule {}
