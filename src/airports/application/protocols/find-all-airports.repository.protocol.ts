import { Airport } from '@airports/domain/airport.entity';
import { AirportRepository } from '@airports/domain/airport.repository';

export interface FindAllAirportsRepository extends AirportRepository {
  findAll(): Promise<Airport[]>;
}
