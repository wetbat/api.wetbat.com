import { Airport } from '@airports/domain/airport.entity';
import { AirportRepository } from '@airports/domain/airport.repository';

export interface FindAirportByIdRepository extends AirportRepository {
  findById(id: string): Promise<Airport>;
}
