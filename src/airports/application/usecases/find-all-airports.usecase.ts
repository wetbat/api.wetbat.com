import { Inject, Injectable } from '@nestjs/common';
import { FindAllAirports } from '@airports/domain/protocols/find-all-airports.usecase.protocol';
import { FindAllAirportsRepository } from '../protocols/find-all-airports.repository.protocol';

@Injectable()
export class FindAllAirportsUseCase implements FindAllAirports {
  constructor(
    @Inject('AirportRepository')
    private readonly airportRepository: FindAllAirportsRepository,
  ) {}

  async exec(): Promise<FindAllAirports.OutputDto> {
    const airports = await this.airportRepository.findAll();
    return {
      airports: airports.map((airport) => ({
        id: airport.getId(),
        name: airport.getName(),
        address: {
          city: airport.getAddress().city,
          country: airport.getAddress().city,
          state: airport.getAddress().state,
          street: airport.getAddress().street,
        },
        location: {
          latitude: airport.getLocation().latitude,
          longitude: airport.getLocation().longitude,
        },
      })),
    };
  }
}
