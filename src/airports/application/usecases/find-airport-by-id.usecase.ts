import { FindAirportById } from '@airports/domain/protocols/find-airport-by-id.usecase.protocol';
import { FindAirportByIdRepository } from '../protocols/find-airport-by-id.repository.protocol';
import { Inject, Injectable } from '@nestjs/common';

@Injectable()
export class FindAirportByIdUseCase implements FindAirportById {
  constructor(
    @Inject('AirportRepository')
    private readonly airportRepository: FindAirportByIdRepository,
  ) {}

  async exec(
    input: FindAirportById.InputDto,
  ): Promise<FindAirportById.OutputDto> {
    const airport = await this.airportRepository.findById(input.id);
    return {
      id: airport.getId(),
      name: airport.getName(),
      address: {
        city: airport.getAddress().city,
        country: airport.getAddress().city,
        state: airport.getAddress().state,
        street: airport.getAddress().street,
      },
      location: {
        latitude: airport.getLocation().latitude,
        longitude: airport.getLocation().longitude,
      },
    };
  }
}
