import { CreateAirport } from '@airports/domain/create-airport';
import { Airport } from '@airports/domain/airport.entity';
import { Location } from '@airports/domain/location.valueobject';
import { Address } from '@airports/domain/address.valueobject';
import { CreateAirportRepository } from '@airports/infra/create-airport.repository';
import { FindAirportByNameRepository } from '@airports/domain/protocols/find-airport-by-name.repository.protocol';

import { Inject, Injectable } from '@nestjs/common';
import { InvalidAirportNameError } from '@airports/domain/errors/InvalidAirportNameError';

@Injectable()
export class CreateAirportUseCase implements CreateAirport {
  constructor(
    @Inject('AirportRepository')
    private readonly airportRepository: CreateAirportRepository &
      FindAirportByNameRepository,
  ) {}

  async exec(input: CreateAirport.InputDto): Promise<CreateAirport.OutputDto> {
    const airportExists = await this.airportRepository.findByName(input.name);
    if (airportExists) {
      throw new InvalidAirportNameError(input.name);
    }
    const location = Location.create(
      input.location.latitude,
      input.location.longitude,
    );
    const address = Address.create(
      input.address.street,
      input.address.city,
      input.address.state,
      input.address.country,
    );
    const airport = Airport.create(input.name, address, location);
    await this.airportRepository.create(airport);
    return {
      id: airport.getId(),
      address: {
        city: airport.getAddress().city,
        country: airport.getAddress().country,
        state: airport.getAddress().state,
        street: airport.getAddress().street,
      },
      location: {
        latitude: airport.getLocation().latitude,
        longitude: airport.getLocation().longitude,
      },
      name: airport.getName(),
    };
  }
}
