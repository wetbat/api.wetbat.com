import { Controller, Get } from '@nestjs/common';
import { FindAllAirportsUseCase } from '@airports/application/usecases/find-all-airports.usecase';
import { ApiTags, ApiOkResponse, ApiProperty } from '@nestjs/swagger';
import { AirportDto } from '@swagger/airport.dto';

export class FindAllAirportsDto {
  @ApiProperty({ type: [AirportDto] })
  airports: AirportDto[];
}

@ApiTags('airports')
@Controller('airports')
export class FindAllAirportsController {
  constructor(
    private readonly findAllAirportsUseCase: FindAllAirportsUseCase,
  ) {}

  @Get()
  @ApiOkResponse({ type: FindAllAirportsDto, description: 'Airport found' })
  async findAll(): Promise<{ airports: AirportDto[] }> {
    const airports = await this.findAllAirportsUseCase.exec();
    return airports;
  }
}
