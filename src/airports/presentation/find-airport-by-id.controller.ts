import { FindAirportByIdUseCase } from '@airports/application/usecases/find-airport-by-id.usecase';

import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { AirportDto } from '@swagger/airport.dto';

@ApiTags('airports')
@Controller('airports')
export class FindAirportByIdController {
  constructor(
    private readonly findAirportByIdUseCase: FindAirportByIdUseCase,
  ) {}

  @Get(':id')
  @ApiOkResponse({ type: AirportDto, description: 'Airport found' })
  async findById(@Param('id') id: string): Promise<AirportDto> {
    const airport = this.findAirportByIdUseCase.exec({ id });
    return airport;
  }
}
