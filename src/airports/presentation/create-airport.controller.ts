import { CreateAirportUseCase } from '@airports/application/usecases/create-airport.usecase';
import { AirportDto } from '@swagger/airport.dto';

import { Controller, Post, Body } from '@nestjs/common';
import {
  ApiTags,
  ApiCreatedResponse,
  ApiBadRequestResponse,
} from '@nestjs/swagger';
import { CreateAirportDto } from '@swagger/create-airport.dto';

@ApiTags('airports')
@Controller('airports')
export class CreateAirportsController {
  constructor(private readonly createAirportUseCase: CreateAirportUseCase) {}

  @Post()
  @ApiCreatedResponse({
    type: AirportDto,
    description: 'The record has been successfully created.',
  })
  @ApiBadRequestResponse({ description: 'Internal Server Error.' })
  async create(
    @Body() createAirportDto: CreateAirportDto,
  ): Promise<AirportDto> {
    const airport = this.createAirportUseCase.exec({
      address: {
        city: createAirportDto.city,
        country: createAirportDto.country,
        state: createAirportDto.state,
        street: createAirportDto.street,
      },
      location: {
        latitude: createAirportDto.latitude,
        longitude: createAirportDto.longitude,
      },
      name: createAirportDto.name,
    });
    return airport;
  }
}
