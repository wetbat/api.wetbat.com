import { CreateAirportRepository } from '@airports/infra/create-airport.repository';
import { Airport } from './airport.entity';
import { AddressSchema } from '@airports/infra/schemas/address.schema';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LocationSchema } from '@airports/infra/schemas/location.schema';
import { AirportSchema } from '@airports/infra/schemas/airport.schema';
import { FindAirportByIdRepository } from './protocols/find-airport-by-id.repository.protocol';
import { Location } from './location.valueobject';
import { Address } from './address.valueobject';
import { FindAirportByNameRepository } from './protocols/find-airport-by-name.repository.protocol';
import { FindAllAirportsRepository } from '@airports/application/protocols/find-all-airports.repository.protocol';

export class AirportMySqlRepository
  implements
    CreateAirportRepository,
    FindAirportByIdRepository,
    FindAirportByNameRepository,
    FindAllAirportsRepository
{
  constructor(
    @InjectRepository(AddressSchema)
    private readonly addressSchemaRepository: Repository<AddressSchema>,
    @InjectRepository(LocationSchema)
    private readonly locationSchemaRepository: Repository<LocationSchema>,
    @InjectRepository(AirportSchema)
    private readonly airportSchemaRepository: Repository<AirportSchema>,
  ) {}

  async findAll(): Promise<Airport[]> {
    const airportSchemas = await this.airportSchemaRepository.find({
      relations: { address: true, location: true },
    });
    return airportSchemas.map((airportSchema) => {
      const address = Address.create(
        airportSchema.address.street,
        airportSchema.address.city,
        airportSchema.address.state,
        airportSchema.address.country,
      );
      const location = Location.create(
        airportSchema.location.latitude,
        airportSchema.location.longitude,
      );
      const airport = Airport.create(
        airportSchema.name,
        address,
        location,
        airportSchema.id,
      );
      return airport;
    });
  }

  async findByName(name: string): Promise<Airport> {
    const airportSchema = await this.airportSchemaRepository.findOne({
      where: { name },
      relations: { address: true, location: true },
    });
    if (!airportSchema) return undefined;
    const address = Address.create(
      airportSchema.address.street,
      airportSchema.address.city,
      airportSchema.address.state,
      airportSchema.address.country,
    );
    const location = Location.create(
      airportSchema.location.latitude,
      airportSchema.location.longitude,
    );
    const airport = Airport.create(
      airportSchema.name,
      address,
      location,
      airportSchema.id,
    );
    return airport;
  }

  async create(airport: Airport): Promise<void> {
    const locationSchema = await this.createLocation(airport.getLocation());
    const addressSchema = await this.createAddress(airport.getAddress());

    const airportSchema = new AirportSchema();
    airportSchema.id = airport.getId();
    airportSchema.name = airport.getName();
    airportSchema.address = addressSchema;
    airportSchema.location = locationSchema;
    await this.airportSchemaRepository.save(airportSchema);
  }

  async findById(id: string): Promise<Airport> {
    const airportSchema = await this.airportSchemaRepository.findOne({
      where: { id },
      relations: { address: true, location: true },
    });
    const airport = Airport.create(
      airportSchema.name,
      Address.create(
        airportSchema.address.street,
        airportSchema.address.city,
        airportSchema.address.state,
        airportSchema.address.country,
      ),
      Location.create(
        airportSchema.location.latitude,
        airportSchema.location.longitude,
      ),
      airportSchema.id,
    );
    return airport;
  }

  private async createLocation(location: Location): Promise<LocationSchema> {
    const locationSchema = LocationSchema.map(location);
    const { identifiers } =
      await this.locationSchemaRepository.insert(locationSchema);
    locationSchema.id = identifiers[0]['id'];
    return locationSchema;
  }

  private async createAddress(address: Address): Promise<AddressSchema> {
    const addressSchema = AddressSchema.map(address);
    const { identifiers } =
      await this.addressSchemaRepository.insert(addressSchema);
    addressSchema.id = identifiers[0]['id'];
    return addressSchema;
  }
}
