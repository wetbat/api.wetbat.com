import { CreateAirport } from '@airports/domain/create-airport';
import { Airport } from '@airports/domain/airport.entity';
import { Location } from '@airports/domain/location.valueobject';
import { Address } from '@airports/domain/address.valueobject';
import { CreateAirportRepository } from '@airports/infra/create-airport.repository';
import { CreateAirportUseCase } from '@airports/application/usecases/create-airport.usecase';
import { FindAirportByNameRepository } from './protocols/find-airport-by-name.repository.protocol';

import { faker } from '@faker-js/faker';
import { InvalidAirportNameError } from './errors/InvalidAirportNameError';

class AirportStubRepository
  implements CreateAirportRepository, FindAirportByNameRepository
{
  async findByName(name: string): Promise<Airport> {
    return airportFaker();
  }
  async create(airport: Airport): Promise<void> {}
}

const airportFaker = (): Airport => {
  const address = Address.create(
    faker.location.secondaryAddress(),
    faker.location.city(),
    faker.location.state(),
    faker.location.country(),
  );
  return Airport.create(
    faker.airline.airport().name,
    address,
    Location.create(faker.location.latitude(), faker.location.longitude()),
  );
};

const createAirportUseCaseFactory = () => {
  const airportStubRepository = new AirportStubRepository();
  const sut = new CreateAirportUseCase(airportStubRepository);
  return { airportStubRepository, sut };
};

const makeInputDto = (): CreateAirport.InputDto => {
  const dto = new CreateAirport.InputDto();
  dto.name = faker.airline.airport().name;
  dto.location = {
    latitude: faker.location.latitude(),
    longitude: faker.location.longitude(),
  };
  dto.address = {
    city: faker.location.city(),
    country: faker.location.country(),
    state: faker.location.state(),
    street: faker.location.secondaryAddress(),
  };
  return dto;
};

describe('CreateAirportUseCase', () => {
  it('should not create repeated airports', async () => {
    const { airportStubRepository, sut } = createAirportUseCaseFactory();
    const input = makeInputDto();
    const address = Address.create(
      input.address.street,
      input.address.city,
      input.address.state,
      input.address.country,
    );
    const airport = Airport.create(
      input.name,
      address,
      Location.create(input.location.latitude, input.location.longitude),
    );
    jest.spyOn(airportStubRepository, 'findByName').mockResolvedValue(airport);

    const response = sut.exec(input);
    await expect(response).rejects.toThrow(
      new InvalidAirportNameError(input.name),
    );
  });
});
