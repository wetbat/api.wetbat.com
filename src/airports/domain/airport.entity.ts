import { Entity } from '@core/entity';
import { Address } from '@airports/domain/address.valueobject';
import { Location } from '@airports/domain/location.valueobject';

export class Airport extends Entity {
  constructor(
    private name: string,
    private address: Address,
    private location: Location,
    id?: string,
  ) {
    super(id);
  }

  public static create(
    name: string,
    address: Address,
    location: Location,
    id?: string,
  ): Airport {
    return new Airport(name, address, location, id);
  }

  public getName(): string {
    return this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public getAddress(): Address {
    return this.address;
  }

  public setAddress(address: Address): void {
    this.address = address;
  }

  public getLocation(): Location {
    return this.location;
  }

  public setLocation(location: Location): void {
    this.location = location;
  }
}
