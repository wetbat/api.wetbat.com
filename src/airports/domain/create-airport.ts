import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface CreateAirport
  extends UseCase<CreateAirport.InputDto, CreateAirport.OutputDto> {}

export namespace CreateAirport {
  export class InputDto extends Dto {
    name: string;
    address: {
      street: string;
      city: string;
      state: string;
      country: string;
    };
    location: {
      latitude: number;
      longitude: number;
    };
  }
  export class OutputDto extends Dto {
    id: string;
    name: string;
    address: {
      street: string;
      city: string;
      state: string;
      country: string;
    };
    location: {
      latitude: number;
      longitude: number;
    };
  }
}
