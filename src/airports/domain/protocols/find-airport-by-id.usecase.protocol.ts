import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface FindAirportById
  extends UseCase<FindAirportById.InputDto, FindAirportById.OutputDto> {}

export namespace FindAirportById {
  export class InputDto extends Dto {
    id: string;
  }
  export class OutputDto extends Dto {
    id: string;
    name: string;
    location: {
      latitude: number;
      longitude: number;
    };
    address: {
      street: string;
      city: string;
      state: string;
      country: string;
    };
  }
}
