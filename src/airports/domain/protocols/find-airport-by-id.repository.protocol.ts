import { Repository } from '@core/repository';
import { Airport } from '@airports/domain/airport.entity';

export interface FindAirportByIdRepository extends Repository<Airport> {
  findById(id: string): Promise<Airport>;
}
