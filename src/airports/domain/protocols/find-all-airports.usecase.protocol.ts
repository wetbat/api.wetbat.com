import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface FindAllAirports
  extends UseCase<undefined, FindAllAirports.OutputDto> {}

export namespace FindAllAirports {
  export class InputDto extends Dto {}
  export class OutputDto extends Dto {
    airports: AirportDto[];
  }
  interface AirportDto {
    name: string;
    id: string;
    location: {
      latitude: number;
      longitude: number;
    };
    address: {
      street: string;
      city: string;
      state: string;
      country: string;
    };
  }
}
