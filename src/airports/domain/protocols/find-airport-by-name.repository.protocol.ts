import { Repository } from '@core/repository';
import { Airport } from '@airports/domain/airport.entity';

export interface FindAirportByNameRepository extends Repository<Airport> {
  findByName(name: string): Promise<Airport>;
}
