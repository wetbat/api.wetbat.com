import { ValueObject } from '@core/valueobject';

export class Location extends ValueObject {
  constructor(
    public readonly latitude: number,
    public readonly longitude: number,
  ) {
    super();
    if (latitude < -90 || latitude > 90) throw new Error('Invalid latitude');
    if (longitude < -180 || longitude > 180)
      throw new Error('Invalid longitude');
  }

  public equal(location: Location): boolean {
    return (
      this.latitude === location.latitude &&
      this.longitude === location.longitude
    );
  }

  public static create(latitude: number, longitude: number): Location {
    return new Location(latitude, longitude);
  }
}
