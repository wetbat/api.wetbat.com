import { ValueObject } from '@core/valueobject';

export class Address extends ValueObject {
  constructor(
    public readonly street: string,
    public readonly city: string,
    public readonly state: string,
    public readonly country: string,
  ) {
    super();
  }

  public equal(address: Address): boolean {
    return (
      this.city === address.city &&
      this.country === address.country &&
      this.state === address.state &&
      this.street === address.street
    );
  }

  public static create(
    street: string,
    city: string,
    state: string,
    country: string,
  ): Address {
    return new Address(street, city, state, country);
  }
}
