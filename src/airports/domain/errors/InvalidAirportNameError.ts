import { DomainError } from '@core/error';

export class InvalidAirportNameError extends DomainError {
  constructor(name: string) {
    super(4, `The airport ${name} alread exists`);
  }
}
