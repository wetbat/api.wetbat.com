import { Repository } from '@core/repository';
import { Airport } from '@airports/domain/airport.entity';

export interface AirportRepository extends Repository<Airport> {}
