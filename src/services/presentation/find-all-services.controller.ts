import { Controller, Get } from '@nestjs/common';
import { FindAllServicesUseCase } from '@services/application/usecases/find-all-services.usecase';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { FindAllServicesDto } from '@swagger/find-all-services.dto';

@ApiTags('services')
@Controller('services')
export class FindAllServicesController {
  constructor(
    private readonly findAllServicesUseCase: FindAllServicesUseCase,
  ) {}

  @Get()
  @ApiOkResponse({ type: FindAllServicesDto })
  findAll(): Promise<FindAllServicesDto> {
    return this.findAllServicesUseCase.exec();
  }
}
