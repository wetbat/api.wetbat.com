import { Controller, Get, Param } from '@nestjs/common';
import { FindServiceByIdUseCase } from '@services/application/usecases/find-service-by-id.usecase';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { ServiceDto } from '@swagger/service.dto';

@ApiTags('services')
@Controller('services')
export class FindServiceByIdController {
  constructor(
    private readonly findServiceByIdUseCase: FindServiceByIdUseCase,
  ) {}

  @Get(':id')
  @ApiOkResponse({ type: ServiceDto })
  findById(@Param('id') id: string): Promise<ServiceDto> {
    return this.findServiceByIdUseCase.exec({ id });
  }
}
