import { Controller, Post, Body } from '@nestjs/common';
import { ConvertQuoteIntoServiceUseCase } from '@services/application/usecases/convert-quote-into-service.usecase';
import { ApiTags, ApiCreatedResponse } from '@nestjs/swagger';
import { ServiceDto } from '@swagger/service.dto';
import { ConvertQuoteDto } from '@swagger/convert-quote.dto';

@ApiTags('services')
@Controller('services')
export class ConvertQuoteIntoServiceController {
  constructor(
    private readonly convertQuoteIntoServiceUseCase: ConvertQuoteIntoServiceUseCase,
  ) {}

  @Post('convert-quote')
  @ApiCreatedResponse({ type: ServiceDto })
  convertQuote(@Body() createQuoteDto: ConvertQuoteDto) {
    return this.convertQuoteIntoServiceUseCase.exec(createQuoteDto);
  }
}
