import { Service } from '@services/domain/service.entity';
import { ServiceRepository } from '@services/domain/service.repository';

export interface FindAllServicesRepository extends ServiceRepository {
  findAll(): Promise<Service[]>;
}
