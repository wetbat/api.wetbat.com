import { Service } from '@services/domain/service.entity';
import { ServiceRepository } from '@services/domain/service.repository';

export interface CreateServiceRepository extends ServiceRepository {
  create(service: Service): Promise<void>;
}
