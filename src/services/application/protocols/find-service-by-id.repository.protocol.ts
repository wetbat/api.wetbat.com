import { Service } from '@services/domain/service.entity';
import { ServiceRepository } from '@services/domain/service.repository';

export interface FindServiceByIdRepository extends ServiceRepository {
  findById(id: string): Promise<Service>;
}
