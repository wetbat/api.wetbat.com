import { Inject, Injectable } from '@nestjs/common';
import { FindQuoteByIdRepository } from '@quotes/application/protocols/find-quote-by-id.repository.protocol';
import { TripQuoteService } from '@quotes/domain/trip-quote.service';
import { ConvertQuoteIntoService } from '@services/domain/convert-quote-into-service.usecase.protocol';
import { Service } from '@services/domain/service.entity';
import { CreateServiceRepository } from '../protocols/create-service.repository.protocol';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { QuoteConvertedIntoServiceEvent } from '@services/domain/quote-converted-into-service.event';

@Injectable()
export class ConvertQuoteIntoServiceUseCase implements ConvertQuoteIntoService {
  constructor(
    @Inject('QuoteRepository')
    private readonly quoteRepository: FindQuoteByIdRepository,
    @Inject('ServiceRepository')
    private readonly serviceRepository: CreateServiceRepository,
    private eventEmitter: EventEmitter2,
  ) {}

  async exec(
    input: ConvertQuoteIntoService.InputDto,
  ): Promise<ConvertQuoteIntoService.OutputDto> {
    const quote = await this.quoteRepository.findById(input.quoteId);
    const price = TripQuoteService.quote(
      quote.getDeparture(),
      quote.getArrival(),
    );
    const service = Service.create(
      quote.getDeparture(),
      quote.getArrival(),
      quote.getTransportation(),
      quote.getTravelers(),
      price,
      quote.getContact(),
    );
    await this.serviceRepository.create(service);
    const quoteConvertedIntoServiceEvent = new QuoteConvertedIntoServiceEvent();
    quoteConvertedIntoServiceEvent.serviceId = service.getId();
    this.eventEmitter.emit('quote.converted', quoteConvertedIntoServiceEvent);
    return {
      price: service.getPrice(),
      orderedAt: service.getOrderedAt(),
      arrival: {
        date: quote.getArrival().date,
        airport: {
          name: quote.getArrival().airport.getName(),
          address: {
            city: quote.getArrival().airport.getAddress().city,
            country: quote.getArrival().airport.getAddress().country,
            state: quote.getArrival().airport.getAddress().state,
            street: quote.getArrival().airport.getAddress().street,
          },
          location: {
            latitude: quote.getArrival().airport.getLocation().latitude,
            longitude: quote.getArrival().airport.getLocation().longitude,
          },
        },
      },
      departure: {
        date: quote.getDeparture().date,
        airport: {
          name: quote.getDeparture().airport.getName(),
          address: {
            city: quote.getDeparture().airport.getAddress().city,
            country: quote.getDeparture().airport.getAddress().country,
            state: quote.getDeparture().airport.getAddress().state,
            street: quote.getDeparture().airport.getAddress().street,
          },
          location: {
            latitude: quote.getDeparture().airport.getLocation().latitude,
            longitude: quote.getDeparture().airport.getLocation().longitude,
          },
        },
      },
      contact: {
        email: quote.getContact().email,
        name: quote.getContact().name,
        phone: quote.getContact().phone,
      },
      id: quote.getId(),
      transportation: quote.getTransportation(),
      travalers: quote.getTravelers(),
    };
  }
}
