import { Inject, Injectable } from '@nestjs/common';
import { FindAllServices } from '@services/domain/find-all-services.usecase.protocol';
import { FindAllServicesRepository } from '../protocols/find-all-services.repository.protocol';

@Injectable()
export class FindAllServicesUseCase implements FindAllServices {
  constructor(
    @Inject('ServiceRepository')
    private readonly serviceRepository: FindAllServicesRepository,
  ) {}

  async exec(): Promise<FindAllServices.OutputDto> {
    const services = await this.serviceRepository.findAll();
    return {
      services: services.map((service) => ({
        price: service.getPrice(),
        orderedAt: service.getOrderedAt(),
        arrival: {
          date: service.getArrival().date,
          airport: {
            id: service.getArrival().airport.getId(),
            name: service.getArrival().airport.getName(),
            address: {
              city: service.getArrival().airport.getAddress().city,
              country: service.getArrival().airport.getAddress().country,
              state: service.getArrival().airport.getAddress().state,
              street: service.getArrival().airport.getAddress().street,
            },
            location: {
              latitude: service.getArrival().airport.getLocation().latitude,
              longitude: service.getArrival().airport.getLocation().longitude,
            },
          },
        },
        departure: {
          date: service.getDeparture().date,
          airport: {
            id: service.getDeparture().airport.getId(),
            name: service.getDeparture().airport.getName(),
            address: {
              city: service.getDeparture().airport.getAddress().city,
              country: service.getDeparture().airport.getAddress().country,
              state: service.getDeparture().airport.getAddress().state,
              street: service.getDeparture().airport.getAddress().street,
            },
            location: {
              latitude: service.getDeparture().airport.getLocation().latitude,
              longitude: service.getDeparture().airport.getLocation().longitude,
            },
          },
        },
        contact: {
          email: service.getContact().email,
          name: service.getContact().name,
          phone: service.getContact().phone,
        },
        id: service.getId(),
        transportation: service.getTransportation(),
        travalers: service.getTravelers(),
      })),
    };
  }
}
