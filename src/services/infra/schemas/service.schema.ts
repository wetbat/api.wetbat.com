import {
  Entity,
  Column,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { DepartureSchema } from '@quotes/infra/schemas/departure.schema';
import { ArrivalSchema } from '@quotes/infra/schemas/arrival.schema';
import { ContactSchema } from '@quotes/infra/schemas/contact.schema';
import { Service } from '@services/domain/service.entity';

@Entity({ name: 'services' })
export class ServiceSchema {
  @PrimaryColumn('uuid')
  id: string;

  @Column()
  transportation: string;

  @Column()
  travalers: number;

  @Column()
  price: number;

  @Column()
  orderedAt: Date;

  @ManyToOne(() => DepartureSchema, (departure) => departure.services)
  departure: DepartureSchema;

  @ManyToOne(() => ArrivalSchema, (arrival) => arrival.services)
  arrival: ArrivalSchema;

  @OneToOne(() => ContactSchema)
  @JoinColumn()
  contact: ContactSchema;

  public static map(service: Service): ServiceSchema {
    const serviceSchema = new ServiceSchema();
    serviceSchema.arrival = ArrivalSchema.map(service.getArrival());
    serviceSchema.contact = ContactSchema.map(service.getContact());
    serviceSchema.departure = DepartureSchema.map(service.getDeparture());
    serviceSchema.transportation = service.getTransportation();
    serviceSchema.travalers = service.getTravelers();
    serviceSchema.price = service.getPrice();
    serviceSchema.orderedAt = service.getOrderedAt();
    return serviceSchema;
  }

  public static create(
    id: string,
    transportation: string,
    travalers: number,
    price: number,
    departure: DepartureSchema,
    arrival: ArrivalSchema,
    contact: ContactSchema,
    orderedAt: Date,
  ): ServiceSchema {
    const serviceSchema = new ServiceSchema();
    serviceSchema.id = id;
    serviceSchema.transportation = transportation;
    serviceSchema.travalers = travalers;
    serviceSchema.departure = departure;
    serviceSchema.arrival = arrival;
    serviceSchema.contact = contact;
    serviceSchema.price = price;
    serviceSchema.orderedAt = orderedAt;
    return serviceSchema;
  }
}
