import { CreateServiceRepository } from '@services/application/protocols/create-service.repository.protocol';
import { ServiceSchema } from './schemas/service.schema';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Service } from '@services/domain/service.entity';
import { Contact } from '@quotes/domain/contact.valueobject';
import {
  ArrivalSchema,
  ContactSchema,
  DepartureSchema,
} from '@quotes/infra/schemas';
import { Arrival } from '@quotes/domain/arrival.valueobject';
import { Departure } from '@quotes/domain/departure.valueobject';
import { FindServiceByIdRepository } from '@services/application/protocols/find-service-by-id.repository.protocol';
import { Airport } from '@airports/domain/airport.entity';
import { Location } from '@airports/domain/location.valueobject';
import { Address } from '@airports/domain/address.valueobject';
import { FindAllServicesRepository } from '@services/application/protocols/find-all-services.repository.protocol';

export class ServiceMysqlRepository
  implements
    CreateServiceRepository,
    FindServiceByIdRepository,
    FindAllServicesRepository
{
  constructor(
    @InjectRepository(ServiceSchema)
    private readonly serviceSchemaRepository: Repository<ServiceSchema>,
    @InjectRepository(ArrivalSchema)
    private readonly arrivalSchemaRepository: Repository<ArrivalSchema>,
    @InjectRepository(ContactSchema)
    private readonly contactSchemaRepository: Repository<ContactSchema>,
    @InjectRepository(DepartureSchema)
    private readonly departureSchemaRepository: Repository<DepartureSchema>,
  ) {}

  async findAll(): Promise<Service[]> {
    const servicesSchema = await this.serviceSchemaRepository.find({
      relations: {
        arrival: {
          airport: { address: true, location: true },
        },
        contact: true,
        departure: {
          airport: { address: true, location: true },
        },
      },
    });
    return servicesSchema.map((serviceSchema) => {
      const arrival = Arrival.create(
        Airport.create(
          serviceSchema.arrival.airport.name,
          Address.create(
            serviceSchema.arrival.airport.address.state,
            serviceSchema.arrival.airport.address.city,
            serviceSchema.arrival.airport.address.state,
            serviceSchema.arrival.airport.address.country,
          ),
          Location.create(
            serviceSchema.arrival.airport.location.latitude,
            serviceSchema.arrival.airport.location.longitude,
          ),
          serviceSchema.arrival.airport.id,
        ),
        serviceSchema.arrival.date,
      );
      const departure = Departure.create(
        Airport.create(
          serviceSchema.departure.airport.name,
          Address.create(
            serviceSchema.departure.airport.address.state,
            serviceSchema.departure.airport.address.city,
            serviceSchema.departure.airport.address.state,
            serviceSchema.departure.airport.address.country,
          ),
          Location.create(
            serviceSchema.departure.airport.location.latitude,
            serviceSchema.departure.airport.location.longitude,
          ),
          serviceSchema.departure.airport.id,
        ),
        serviceSchema.departure.date,
      );
      const contact = Contact.create(
        serviceSchema.contact.name,
        serviceSchema.contact.phone,
        serviceSchema.contact.email,
      );
      const service = Service.create(
        departure,
        arrival,
        serviceSchema.transportation,
        serviceSchema.travalers,
        serviceSchema.price,
        contact,
        serviceSchema.id,
      );
      service.setOrderedAt(serviceSchema.orderedAt);
      return service;
    });
  }

  async findById(id: string): Promise<Service> {
    const serviceSchema = await this.serviceSchemaRepository.findOneOrFail({
      where: { id },
      relations: {
        arrival: {
          airport: { address: true, location: true },
        },
        contact: true,
        departure: {
          airport: { address: true, location: true },
        },
      },
    });
    const arrival = Arrival.create(
      Airport.create(
        serviceSchema.arrival.airport.name,
        Address.create(
          serviceSchema.arrival.airport.address.state,
          serviceSchema.arrival.airport.address.city,
          serviceSchema.arrival.airport.address.state,
          serviceSchema.arrival.airport.address.country,
        ),
        Location.create(
          serviceSchema.arrival.airport.location.latitude,
          serviceSchema.arrival.airport.location.longitude,
        ),
        serviceSchema.arrival.airport.id,
      ),
      serviceSchema.arrival.date,
    );
    const departure = Departure.create(
      Airport.create(
        serviceSchema.departure.airport.name,
        Address.create(
          serviceSchema.departure.airport.address.state,
          serviceSchema.departure.airport.address.city,
          serviceSchema.departure.airport.address.state,
          serviceSchema.departure.airport.address.country,
        ),
        Location.create(
          serviceSchema.departure.airport.location.latitude,
          serviceSchema.departure.airport.location.longitude,
        ),
        serviceSchema.departure.airport.id,
      ),
      serviceSchema.departure.date,
    );
    const contact = Contact.create(
      serviceSchema.contact.name,
      serviceSchema.contact.phone,
      serviceSchema.contact.email,
    );
    const service = Service.create(
      departure,
      arrival,
      serviceSchema.transportation,
      serviceSchema.travalers,
      serviceSchema.price,
      contact,
      serviceSchema.id,
    );
    service.setOrderedAt(serviceSchema.orderedAt);
    return service;
  }

  async create(service: Service): Promise<void> {
    const contactSchema = await this.createContact(service.getContact());
    const departureSchema = await this.createDeparture(service.getDeparture());
    const arrivalSchema = await this.createArrival(service.getArrival());
    const serviceSchema = ServiceSchema.create(
      service.getId(),
      service.getTransportation(),
      service.getTravelers(),
      service.getPrice(),
      departureSchema,
      arrivalSchema,
      contactSchema,
      service.getOrderedAt(),
    );
    await this.serviceSchemaRepository.insert(serviceSchema);
  }

  private async createContact(contact: Contact): Promise<ContactSchema> {
    const contactSchema = ContactSchema.map(contact);
    const { identifiers } =
      await this.contactSchemaRepository.insert(contactSchema);
    contactSchema.id = identifiers[0]['id'] as unknown as string;
    return contactSchema;
  }

  private async createArrival(arrival: Arrival): Promise<ArrivalSchema> {
    const arrivalSchema = ArrivalSchema.map(arrival);
    const { identifiers } =
      await this.arrivalSchemaRepository.insert(arrivalSchema);
    arrivalSchema.id = identifiers[0]['id'] as unknown as string;
    return arrivalSchema;
  }

  private async createDeparture(
    departure: Departure,
  ): Promise<DepartureSchema> {
    const departureSchema = DepartureSchema.map(departure);
    const { identifiers } =
      await this.departureSchemaRepository.insert(departureSchema);
    departureSchema.id = identifiers[0]['id'] as unknown as string;
    return departureSchema;
  }
}
