import { Repository } from '@core/repository';
import { Service } from '@services/domain/service.entity';

export interface ServiceRepository extends Repository<Service> {}
