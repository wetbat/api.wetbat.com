import { Entity } from '@core/entity';
import { Arrival } from '@quotes/domain/arrival.valueobject';
import { Departure } from '@quotes/domain/departure.valueobject';
import { Contact } from '@quotes/domain/contact.valueobject';

export class Service extends Entity {
  private orderedAt: Date;

  constructor(
    private departure: Departure,
    private arrival: Arrival,
    private transportation: string,
    private travalers: number,
    private price: number,
    private contact: Contact,
    id?: string,
  ) {
    super(id);
    this.orderedAt = new Date();
  }

  public static create(
    departure: Departure,
    arrival: Arrival,
    transportation: string,
    travalers: number,
    price: number,
    contact: Contact,
    id?: string,
  ): Service {
    return new Service(
      departure,
      arrival,
      transportation,
      travalers,
      price,
      contact,
      id,
    );
  }

  public getDeparture(): Departure {
    return this.departure;
  }

  public getArrival(): Arrival {
    return this.arrival;
  }

  public getTransportation(): string {
    return this.transportation;
  }

  public getTravelers(): number {
    return this.travalers;
  }

  public getContact(): Contact {
    return this.contact;
  }

  public setDeparture(departure: Departure): void {
    this.departure = departure;
  }

  public setArrival(arrival: Arrival): void {
    this.arrival = arrival;
  }

  public setTransportation(transportation: string): void {
    this.transportation = transportation;
  }

  public setTravelers(travalers: number): void {
    this.travalers = travalers;
  }

  public setContact(contact: Contact): void {
    this.contact = contact;
  }

  public getPrice(): number {
    return this.price;
  }

  public setPrice(price: number): void {
    this.price = price;
  }

  public getOrderedAt(): Date {
    return this.orderedAt;
  }

  public setOrderedAt(orderedAt: Date): void {
    this.orderedAt = orderedAt;
  }
}
