import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface ConvertQuoteIntoService
  extends UseCase<
    ConvertQuoteIntoService.InputDto,
    ConvertQuoteIntoService.OutputDto
  > {}

export namespace ConvertQuoteIntoService {
  export class InputDto extends Dto {
    quoteId: string;
  }
  export class OutputDto extends Dto {
    id: string;
    price: number;
    orderedAt: Date;
    departure: {
      date: Date;
      airport: {
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    arrival: {
      date: Date;
      airport: {
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    travalers: number;
    transportation: string;
    contact: {
      name: string;
      phone: string;
      email: string;
    };
  }
}
