import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface FindAllServices
  extends UseCase<undefined, FindAllServices.OutputDto> {}

export namespace FindAllServices {
  export class InputDto extends Dto {
    id: string;
  }
  export class ServiceDto extends Dto {
    id: string;
    price: number;
    orderedAt: Date;
    departure: {
      date: Date;
      airport: {
        id: string;
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    arrival: {
      date: Date;
      airport: {
        id: string;
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    travalers: number;
    transportation: string;
    contact: {
      name: string;
      phone: string;
      email: string;
    };
  }
  export class OutputDto extends Dto {
    services: ServiceDto[];
  }
}
