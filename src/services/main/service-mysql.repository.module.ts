import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  ArrivalSchema,
  ContactSchema,
  DepartureSchema,
} from '@quotes/infra/schemas';
import { ServiceSchema } from '@services/infra/schemas/service.schema';
import { ServiceMysqlRepository } from '@services/infra/service-mysq.repository';

@Module({
  providers: [
    {
      provide: 'ServiceRepository',
      useClass: ServiceMysqlRepository,
    },
  ],
  exports: [
    TypeOrmModule.forFeature([
      ArrivalSchema,
      ContactSchema,
      DepartureSchema,
      ServiceSchema,
    ]),
    'ServiceRepository',
  ],
  imports: [
    TypeOrmModule.forFeature([
      ArrivalSchema,
      ContactSchema,
      DepartureSchema,
      ServiceSchema,
    ]),
  ],
})
export class ServiceMySqlRepositoryModule {}
