import { Module } from '@nestjs/common';
import { ConvertQuoteIntoServiceController } from '../presentation/convert-quote.controller';
import { QuoteMysqlRepository } from '@quotes/infra/quote-mysq.repository';
import { ServiceMySqlRepositoryModule } from './service-mysql.repository.module';
import { AirportRepositoryModule } from '@airports/main/airport.repository.module';
import { QuoteMySqlRepositoryModule } from '@quotes/main/quote-mysql.repository.module';
import { ConvertQuoteIntoServiceUseCase } from '@services/application/usecases/convert-quote-into-service.usecase';
import { ServiceMysqlRepository } from '@services/infra/service-mysq.repository';
import { FindServiceByIdController } from '@services/presentation/find-service-by-id.controller';
import { FindServiceByIdUseCase } from '@services/application/usecases/find-service-by-id.usecase';
import { FindAllServicesController } from '@services/presentation/find-all-services.controller';
import { FindAllServicesUseCase } from '@services/application/usecases/find-all-services.usecase';

@Module({
  controllers: [
    ConvertQuoteIntoServiceController,
    FindServiceByIdController,
    FindAllServicesController,
  ],
  providers: [
    QuoteMysqlRepository,
    ServiceMysqlRepository,
    ConvertQuoteIntoServiceUseCase,
    FindServiceByIdUseCase,
    FindAllServicesUseCase,
  ],
  imports: [
    ServiceMySqlRepositoryModule,
    AirportRepositoryModule,
    QuoteMySqlRepositoryModule,
  ],
})
export class ServicesModule {}
