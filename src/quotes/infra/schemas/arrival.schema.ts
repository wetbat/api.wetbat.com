import {
  Entity,
  Column,
  JoinColumn,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { AirportSchema } from '@airports/infra/schemas/airport.schema';
import { Arrival } from '@quotes/domain/arrival.valueobject';
import { QuoteSchema } from '.';
import { ServiceSchema } from '@services/infra/schemas/service.schema';

@Entity({ name: 'arrivals' })
export class ArrivalSchema {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  date: Date;

  @ManyToOne(() => AirportSchema, (airport) => airport.arrivals)
  airport: AirportSchema;

  @OneToMany(() => QuoteSchema, (quote) => quote.arrival)
  quotes: QuoteSchema[];

  @OneToMany(() => ServiceSchema, (service) => service.arrival)
  services: ServiceSchema[];

  public static map(arrival: Arrival): ArrivalSchema {
    const arrivalSchema = new ArrivalSchema();
    arrivalSchema.date = arrival.date;
    arrivalSchema.airport = AirportSchema.map(arrival.airport);
    return arrivalSchema;
  }
}
