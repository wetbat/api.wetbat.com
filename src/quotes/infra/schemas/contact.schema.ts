import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Contact } from '@quotes/domain/contact.valueobject';

@Entity({ name: 'contacts' })
export class ContactSchema {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column()
  email: string;

  public static map(contact: Contact): ContactSchema {
    const contactSchema = new ContactSchema();
    contactSchema.email = contact.email;
    contactSchema.name = contact.name;
    contactSchema.phone = contact.phone;
    return contactSchema;
  }
}
