import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { AirportSchema } from '@airports/infra/schemas/airport.schema';
import { Departure } from '@quotes/domain/departure.valueobject';
import { QuoteSchema } from '.';
import { ServiceSchema } from '@services/infra/schemas/service.schema';

@Entity({ name: 'departures' })
export class DepartureSchema {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  date: Date;

  @ManyToOne(() => AirportSchema, (airport) => airport.departures)
  airport: AirportSchema;

  @OneToMany(() => QuoteSchema, (quote) => quote.departure)
  quotes: QuoteSchema[];

  @OneToMany(() => ServiceSchema, (service) => service.departure)
  services: ServiceSchema[];

  public static map(departure: Departure): DepartureSchema {
    const departureSchema = new DepartureSchema();
    departureSchema.date = departure.date;
    departureSchema.airport = AirportSchema.map(departure.airport);
    return departureSchema;
  }
}
