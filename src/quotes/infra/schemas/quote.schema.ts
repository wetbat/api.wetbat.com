import {
  Entity,
  Column,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { DepartureSchema } from './departure.schema';
import { ArrivalSchema } from './arrival.schema';
import { ContactSchema } from './contact.schema';
import { Quote } from '@quotes/domain/quote.entity';

@Entity({ name: 'quotes' })
export class QuoteSchema {
  @PrimaryColumn()
  id: string;

  @Column()
  transportation: string;

  @Column()
  travalers: number;

  @ManyToOne(() => ArrivalSchema, (arrival) => arrival.quotes)
  @JoinColumn({ name: 'arrivalId' })
  arrival: ArrivalSchema;

  @ManyToOne(() => DepartureSchema, (departure) => departure.quotes)
  @JoinColumn({ name: 'departureId' })
  departure: DepartureSchema;

  @OneToOne(() => ContactSchema)
  @JoinColumn()
  contact: ContactSchema;

  public static map(quote: Quote): QuoteSchema {
    const quoteSchema = new QuoteSchema();
    quoteSchema.arrival = ArrivalSchema.map(quote.getArrival());
    quoteSchema.contact = ContactSchema.map(quote.getContact());
    quoteSchema.departure = DepartureSchema.map(quote.getDeparture());
    quoteSchema.transportation = quote.getTransportation();
    quoteSchema.travalers = quote.getTravelers();
    return quoteSchema;
  }

  public static create(
    id: string,
    transportation: string,
    travalers: number,
    departure: DepartureSchema,
    arrival: ArrivalSchema,
    contact: ContactSchema,
  ): QuoteSchema {
    const quoteSchema = new QuoteSchema();
    quoteSchema.id = id;
    quoteSchema.transportation = transportation;
    quoteSchema.travalers = travalers;
    quoteSchema.departure = departure;
    quoteSchema.arrival = arrival;
    quoteSchema.contact = contact;
    return quoteSchema;
  }
}
