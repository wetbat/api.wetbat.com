import { ContactSchema } from '@quotes/infra/schemas/contact.schema';
import { CreateQuoteRepository } from '@quotes/application/protocols/create-quote.repository.protocol';
import { Quote } from '@quotes/domain/quote.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ArrivalSchema } from '@quotes/infra/schemas/arrival.schema';
import { DepartureSchema } from '@quotes/infra/schemas/departure.schema';
import { QuoteSchema } from '@quotes/infra/schemas/quote.schema';
import { Contact } from '@quotes/domain/contact.valueobject';
import { Arrival } from '@quotes/domain/arrival.valueobject';
import { Departure } from '@quotes/domain/departure.valueobject';
import { FindQuoteByIdRepository } from '@quotes/application/protocols/find-quote-by-id.repository.protocol';
import { Address } from '@airports/domain/address.valueobject';
import { Airport } from '@airports/domain/airport.entity';
import { Location } from '@airports/domain/location.valueobject';
import { FindAllQuotesRepository } from '@quotes/application/protocols/find-all-quote.repository.protocol';

export class QuoteMysqlRepository
  implements
    CreateQuoteRepository,
    FindQuoteByIdRepository,
    FindAllQuotesRepository
{
  constructor(
    @InjectRepository(ContactSchema)
    private readonly contactSchemaRepository: Repository<ContactSchema>,
    @InjectRepository(ArrivalSchema)
    private readonly arrivalSchemaRepository: Repository<ArrivalSchema>,
    @InjectRepository(DepartureSchema)
    private readonly departureSchemaRepository: Repository<DepartureSchema>,
    @InjectRepository(QuoteSchema)
    private readonly quoteSchemaRepository: Repository<QuoteSchema>,
  ) {}

  async findById(id: string): Promise<Quote> {
    const quoteSchema = await this.quoteSchemaRepository.findOneOrFail({
      where: { id },
      relations: {
        arrival: {
          airport: {
            address: true,
            location: true,
          },
        },
        departure: {
          airport: {
            address: true,
            location: true,
          },
        },
        contact: true,
      },
    });
    const arrival = Arrival.create(
      Airport.create(
        quoteSchema.arrival.airport.name,
        Address.create(
          quoteSchema.arrival.airport.address.state,
          quoteSchema.arrival.airport.address.city,
          quoteSchema.arrival.airport.address.state,
          quoteSchema.arrival.airport.address.country,
        ),
        Location.create(
          quoteSchema.arrival.airport.location.latitude,
          quoteSchema.arrival.airport.location.longitude,
        ),
        quoteSchema.arrival.airport.id,
      ),
      quoteSchema.arrival.date,
    );
    const departure = Departure.create(
      Airport.create(
        quoteSchema.departure.airport.name,
        Address.create(
          quoteSchema.departure.airport.address.state,
          quoteSchema.departure.airport.address.city,
          quoteSchema.departure.airport.address.state,
          quoteSchema.departure.airport.address.country,
        ),
        Location.create(
          quoteSchema.departure.airport.location.latitude,
          quoteSchema.departure.airport.location.longitude,
        ),
        quoteSchema.departure.airport.id,
      ),
      quoteSchema.departure.date,
    );
    const contact = Contact.create(
      quoteSchema.contact.name,
      quoteSchema.contact.phone,
      quoteSchema.contact.email,
    );
    return Quote.create(
      departure,
      arrival,
      quoteSchema.transportation,
      quoteSchema.travalers,
      contact,
    );
  }

  async findAll(): Promise<Quote[]> {
    const quoteSchemas = await this.quoteSchemaRepository.find({
      relations: {
        arrival: {
          airport: {
            address: true,
            location: true,
          },
        },
        departure: {
          airport: {
            address: true,
            location: true,
          },
        },
        contact: true,
      },
    });
    return quoteSchemas.map((quoteSchema) => {
      const arrival = Arrival.create(
        Airport.create(
          quoteSchema.arrival.airport.name,
          Address.create(
            quoteSchema.arrival.airport.address.state,
            quoteSchema.arrival.airport.address.city,
            quoteSchema.arrival.airport.address.state,
            quoteSchema.arrival.airport.address.country,
          ),
          Location.create(
            quoteSchema.arrival.airport.location.latitude,
            quoteSchema.arrival.airport.location.longitude,
          ),
          quoteSchema.arrival.airport.id,
        ),
        quoteSchema.arrival.date,
      );
      const departure = Departure.create(
        Airport.create(
          quoteSchema.departure.airport.name,
          Address.create(
            quoteSchema.departure.airport.address.state,
            quoteSchema.departure.airport.address.city,
            quoteSchema.departure.airport.address.state,
            quoteSchema.departure.airport.address.country,
          ),
          Location.create(
            quoteSchema.departure.airport.location.latitude,
            quoteSchema.departure.airport.location.longitude,
          ),
          quoteSchema.departure.airport.id,
        ),
        quoteSchema.departure.date,
      );
      const contact = Contact.create(
        quoteSchema.contact.name,
        quoteSchema.contact.phone,
        quoteSchema.contact.email,
      );
      const quote = Quote.create(
        departure,
        arrival,
        quoteSchema.transportation,
        quoteSchema.travalers,
        contact,
        quoteSchema.id,
      );
      return quote;
    });
  }

  async create(quote: Quote): Promise<void> {
    const contactSchema = await this.createContact(quote.getContact());
    const departureSchema = await this.createDeparture(quote.getDeparture());
    const arrivalSchema = await this.createArrival(quote.getArrival());
    const quoteSchema = QuoteSchema.create(
      quote.getId(),
      quote.getTransportation(),
      quote.getTravelers(),
      departureSchema,
      arrivalSchema,
      contactSchema,
    );
    await this.quoteSchemaRepository.insert(quoteSchema);
  }

  private async createContact(contact: Contact): Promise<ContactSchema> {
    const contactSchema = ContactSchema.map(contact);
    const { identifiers } =
      await this.contactSchemaRepository.insert(contactSchema);
    contactSchema.id = identifiers[0]['id'] as unknown as string;
    return contactSchema;
  }

  private async createArrival(arrival: Arrival): Promise<ArrivalSchema> {
    const arrivalSchema = ArrivalSchema.map(arrival);
    const { identifiers } =
      await this.arrivalSchemaRepository.insert(arrivalSchema);
    arrivalSchema.id = identifiers[0]['id'] as unknown as string;
    return arrivalSchema;
  }

  private async createDeparture(
    departure: Departure,
  ): Promise<DepartureSchema> {
    const departureSchema = DepartureSchema.map(departure);
    const { identifiers } =
      await this.departureSchemaRepository.insert(departureSchema);
    departureSchema.id = identifiers[0]['id'] as unknown as string;
    return departureSchema;
  }
}
