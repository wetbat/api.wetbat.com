import { Inject, Injectable } from '@nestjs/common';
import { FindQuoteByIdRepository } from '../protocols/find-quote-by-id.repository.protocol';
import { TripQuote } from '@quotes/domain/trip-quote.usecase.protocol';
import { TripQuoteService } from '@quotes/domain/trip-quote.service';

@Injectable()
export class TripQuoteUseCase implements TripQuote {
  constructor(
    @Inject('QuoteRepository')
    private readonly quoteRepository: FindQuoteByIdRepository,
  ) {}

  async exec(input: TripQuote.InputDto): Promise<TripQuote.OutputDto> {
    const quote = await this.quoteRepository.findById(input.quoteId);
    const price = TripQuoteService.quote(
      quote.getDeparture(),
      quote.getArrival(),
    );
    return {
      price,
    };
  }
}
