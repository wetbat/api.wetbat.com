import { Inject, Injectable } from '@nestjs/common';
import { FindQuoteById } from '@quotes/domain/find-quote-by-id.usecase.protocol';
import { FindQuoteByIdRepository } from '../protocols/find-quote-by-id.repository.protocol';

@Injectable()
export class FindQuoteByIdUseCase implements FindQuoteById {
  constructor(
    @Inject('QuoteRepository')
    private readonly quoteRepository: FindQuoteByIdRepository,
  ) {}

  async exec(input: FindQuoteById.InputDto): Promise<FindQuoteById.OutputDto> {
    const quote = await this.quoteRepository.findById(input.id);
    return {
      arrival: {
        date: quote.getArrival().date,
        airport: {
          id: quote.getArrival().airport.getId(),
          name: quote.getArrival().airport.getName(),
          address: {
            city: quote.getArrival().airport.getAddress().city,
            country: quote.getArrival().airport.getAddress().country,
            state: quote.getArrival().airport.getAddress().state,
            street: quote.getArrival().airport.getAddress().street,
          },
          location: {
            latitude: quote.getArrival().airport.getLocation().latitude,
            longitude: quote.getArrival().airport.getLocation().longitude,
          },
        },
      },
      departure: {
        date: quote.getDeparture().date,
        airport: {
          id: quote.getArrival().airport.getId(),
          name: quote.getDeparture().airport.getName(),
          address: {
            city: quote.getDeparture().airport.getAddress().city,
            country: quote.getDeparture().airport.getAddress().country,
            state: quote.getDeparture().airport.getAddress().state,
            street: quote.getDeparture().airport.getAddress().street,
          },
          location: {
            latitude: quote.getDeparture().airport.getLocation().latitude,
            longitude: quote.getDeparture().airport.getLocation().longitude,
          },
        },
      },
      contact: {
        email: quote.getContact().email,
        name: quote.getContact().name,
        phone: quote.getContact().phone,
      },
      id: quote.getId(),
      transportation: quote.getTransportation(),
      travalers: quote.getTravelers(),
    };
  }
}
