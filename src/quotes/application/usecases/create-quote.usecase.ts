import { CreateQuote } from '@quotes/domain/create-quote.usecase.protocol';
import { Quote } from '@quotes/domain/quote.entity';
import { Departure } from '@quotes/domain/departure.valueobject';
import { FindAirportByIdRepository } from '@airports/domain/protocols/find-airport-by-id.repository.protocol';
import { Inject } from '@nestjs/common';
import { Arrival } from '@quotes/domain/arrival.valueobject';
import { Contact } from '@quotes/domain/contact.valueobject';
import { CreateQuoteRepository } from '@quotes/application/protocols/create-quote.repository.protocol';

export class CreateQuoteUseCase implements CreateQuote {
  constructor(
    @Inject('AirportRepository')
    private readonly airportRepository: FindAirportByIdRepository,
    @Inject('QuoteRepository')
    private readonly quoteRepository: CreateQuoteRepository,
  ) {}

  async exec(input: CreateQuote.InputDto): Promise<Quote> {
    const departureAirport = await this.airportRepository.findById(
      input.departure.airportId,
    );
    const arrivalAirport = await this.airportRepository.findById(
      input.arrival.airportId,
    );
    const arrivalDate: Date = new Date(
      new Date(input.departure.date).getTime() +
        Math.sqrt(
          Math.pow(
            departureAirport.getLocation().latitude -
              arrivalAirport.getLocation().latitude,
            2,
          ) +
            Math.pow(
              departureAirport.getLocation().longitude -
                arrivalAirport.getLocation().longitude,
              2,
            ),
        ) *
          600000,
    );
    const quote = Quote.create(
      Departure.create(departureAirport, new Date(input.departure.date)),
      Arrival.create(arrivalAirport, arrivalDate),
      input.transportation,
      input.travalers,
      Contact.create(
        input.contact.name,
        input.contact.phone,
        input.contact.email,
      ),
    );
    await this.quoteRepository.create(quote);
    return quote;
  }
}
