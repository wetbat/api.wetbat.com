import { Quote } from '@quotes/domain/quote.entity';
import { QuoteRepository } from '@quotes/domain/quote.repository';

export interface FindAllQuotesRepository extends QuoteRepository {
  findAll(): Promise<Quote[]>;
}
