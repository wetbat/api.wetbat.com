import { Quote } from '@quotes/domain/quote.entity';
import { QuoteRepository } from '@quotes/domain/quote.repository';

export interface CreateQuoteRepository extends QuoteRepository {
  create(quote: Quote): Promise<void>;
}
