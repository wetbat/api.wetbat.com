import { Quote } from '@quotes/domain/quote.entity';
import { QuoteRepository } from '@quotes/domain/quote.repository';

export interface FindQuoteByIdRepository extends QuoteRepository {
  findById(id: string): Promise<Quote>;
}
