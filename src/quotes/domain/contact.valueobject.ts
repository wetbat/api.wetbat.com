import { ValueObject } from '@core/valueobject';

export class Contact extends ValueObject {
  constructor(
    public readonly name: string,
    public readonly phone: string,
    public readonly email: string,
  ) {
    super();
  }

  public equal(contact: Contact): boolean {
    return (
      contact.name === this.name &&
      contact.phone === this.phone &&
      contact.email === this.email
    );
  }

  public static create(name: string, phone: string, email: string): Contact {
    return new Contact(name, phone, email);
  }
}
