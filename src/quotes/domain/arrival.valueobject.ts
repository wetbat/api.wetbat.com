import { Airport } from '@airports/domain/airport.entity';
import { ValueObject } from '@core/valueobject';

export class Arrival extends ValueObject {
  constructor(
    public readonly airport: Airport,
    public readonly date: Date,
  ) {
    super();
  }

  equal(arrival: Arrival): boolean {
    return (
      arrival.date.toString() === this.date.toString() &&
      this.airport.getId() === arrival.airport.getId()
    );
  }

  public static create(airport: Airport, date: Date): Arrival {
    return new Arrival(airport, date);
  }
}
