import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface TripQuote
  extends UseCase<TripQuote.InputDto, TripQuote.OutputDto> {}

export namespace TripQuote {
  export class InputDto extends Dto {
    quoteId: string;
  }
  export class OutputDto extends Dto {
    price: number;
  }
}
