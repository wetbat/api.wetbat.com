import { Airport } from '@airports/domain/airport.entity';
import { ValueObject } from '@core/valueobject';

export class Departure extends ValueObject {
  constructor(
    public readonly airport: Airport,
    public readonly date: Date,
  ) {
    super();
  }

  equal(departure: Departure): boolean {
    return (
      departure.date.toString() === this.date.toString() &&
      this.airport.getId() === departure.airport.getId()
    );
  }

  public static create(airport: Airport, date: Date): Departure {
    return new Departure(airport, date);
  }
}
