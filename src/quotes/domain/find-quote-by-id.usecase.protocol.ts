import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface FindQuoteById
  extends UseCase<FindQuoteById.InputDto, FindQuoteById.OutputDto> {}

export namespace FindQuoteById {
  export class InputDto extends Dto {
    id: string;
  }
  export class OutputDto extends Dto {
    id: string;
    departure: {
      date: Date;
      airport: {
        id: string;
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    arrival: {
      date: Date;
      airport: {
        id: string;
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    travalers: number;
    transportation: string;
    contact: {
      name: string;
      phone: string;
      email: string;
    };
  }
}
