import { Arrival } from './arrival.valueobject';
import { Departure } from './departure.valueobject';

export class TripQuoteService {
  public static quote(departure: Departure, arrival: Arrival): number {
    const distance = Math.sqrt(
      Math.pow(
        departure.airport.getLocation().latitude -
          arrival.airport.getLocation().latitude,
        2,
      ) +
        Math.pow(
          departure.airport.getLocation().longitude -
            arrival.airport.getLocation().longitude,
          2,
        ),
    );
    const price =
      Math.round(100 * distance * (15 + 1 - 2 * Math.random())) / 100;
    return price;
  }
}
