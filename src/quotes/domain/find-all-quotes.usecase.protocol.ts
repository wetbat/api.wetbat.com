import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';

export interface FindAllQuotes
  extends UseCase<undefined, FindAllQuotes.OutputDto> {}

export namespace FindAllQuotes {
  export class OutputDto extends Dto {
    quotes: QuoteDto[];
  }
  type QuoteDto = {
    id: string;
    departure: {
      date: Date;
      airport: {
        id: string;
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    arrival: {
      date: Date;
      airport: {
        id: string;
        name: string;
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        };
        location: {
          latitude: number;
          longitude: number;
        };
      };
    };
    travalers: number;
    transportation: string;
    contact: {
      name: string;
      phone: string;
      email: string;
    };
  };
}
