import { Entity } from '@core/entity';
import { Arrival } from '@quotes/domain/arrival.valueobject';
import { Departure } from '@quotes/domain/departure.valueobject';
import { InvalidQuoteDate } from './errors/invalid-quote-date.error';
import { InvalidQuoteTravelers } from './errors/invalid-quote-travelers.error';
import { InvalidQuoteArrival } from './errors/invalid-quote-arrival.error';
import { Contact } from './contact.valueobject';

export class Quote extends Entity {
  constructor(
    private departure: Departure,
    private arrival: Arrival,
    private transportation: string,
    private travalers: number,
    private contact: Contact,
    id?: string,
  ) {
    super(id);
    if (arrival.date.getTime() <= departure.date.getTime()) {
      throw new InvalidQuoteDate(departure.date, arrival.date);
    }

    if (travalers <= 0) {
      throw new InvalidQuoteTravelers(travalers);
    }

    if (departure.airport.equal(arrival.airport)) {
      throw new InvalidQuoteArrival(arrival);
    }
  }

  public static create(
    departure: Departure,
    arrival: Arrival,
    transportation: string,
    travalers: number,
    contact: Contact,
    id?: string,
  ): Quote {
    return new Quote(
      departure,
      arrival,
      transportation,
      travalers,
      contact,
      id,
    );
  }

  public getDeparture(): Departure {
    return this.departure;
  }

  public getArrival(): Arrival {
    return this.arrival;
  }

  public getTransportation(): string {
    return this.transportation;
  }

  public getTravelers(): number {
    return this.travalers;
  }

  public getContact(): Contact {
    return this.contact;
  }

  public setDeparture(departure: Departure): void {
    this.departure = departure;
  }

  public setArrival(arrival: Arrival): void {
    this.arrival = arrival;
  }

  public setTransportation(transportation: string): void {
    this.transportation = transportation;
  }

  public setTravelers(travalers: number): void {
    this.travalers = travalers;
  }

  public setContact(contact: Contact): void {
    this.contact = contact;
  }
}
