import { Dto } from '@core/dto';
import { UseCase } from '@core/usecase';
import { Quote } from '@quotes/domain/quote.entity';

export interface CreateQuote extends UseCase<CreateQuote.InputDto, Quote> {}

export namespace CreateQuote {
  export class InputDto extends Dto {
    departure: {
      date: Date;
      airportId: string;
    };
    arrival: {
      airportId: string;
    };
    travalers: number;
    transportation: string;
    contact: {
      name: string;
      phone: string;
      email: string;
    };
  }
  export class OutputDto extends Dto {
    id: string;
    departure: {
      date: Date;
      airport: {
        name: string;
        address: string;
      };
    };
    arrival: {
      airport: {
        name: string;
        address: string;
      };
    };
    travalers: number;
    transportation: string;
    contact: {
      name: string;
      phone: string;
      email: string;
    };
  }
}
