import { Repository } from '@core/repository';
import { Quote } from './quote.entity';

export interface QuoteRepository extends Repository<Quote> {}
