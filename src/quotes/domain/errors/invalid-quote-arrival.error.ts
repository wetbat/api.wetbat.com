import { DomainError } from '@core/error';
import { Arrival } from '../arrival.valueobject';

export class InvalidQuoteArrival extends DomainError {
  constructor(arrival: Arrival) {
    super(
      3,
      `The arrival cannot be the same as departure: ${arrival.airport.getName()}`,
    );
  }
}
