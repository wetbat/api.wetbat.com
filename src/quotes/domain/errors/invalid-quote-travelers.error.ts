import { DomainError } from '@core/error';

export class InvalidQuoteTravelers extends DomainError {
  constructor(travelers: number) {
    super(
      2,
      `The travelers cannot be less than 1. Travelers informed: ${travelers}`,
    );
  }
}
