import { DomainError } from '@core/error';

export class InvalidQuoteDate extends DomainError {
  constructor(departureDate: Date, arrivalDate: Date) {
    super(
      1,
      `The arrival date ${arrivalDate.toISOString()} cannot be less than departure date ${departureDate.toISOString()}`,
    );
  }
}
