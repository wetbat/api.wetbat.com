import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  ArrivalSchema,
  ContactSchema,
  DepartureSchema,
  QuoteSchema,
} from '@quotes/infra/schemas';
import { QuoteMysqlRepository } from '@quotes/infra/quote-mysq.repository';

@Module({
  providers: [
    {
      provide: 'QuoteRepository',
      useClass: QuoteMysqlRepository,
    },
  ],
  exports: [
    TypeOrmModule.forFeature([
      ArrivalSchema,
      ContactSchema,
      DepartureSchema,
      QuoteSchema,
    ]),
    'QuoteRepository',
  ],
  imports: [
    TypeOrmModule.forFeature([
      ArrivalSchema,
      ContactSchema,
      DepartureSchema,
      QuoteSchema,
    ]),
  ],
})
export class QuoteMySqlRepositoryModule {}
