import { Module } from '@nestjs/common';
import { CreateQuoteController } from '../presentation/create-quote.controller';
import { CreateQuoteUseCase } from '@quotes/application/usecases/create-quote.usecase';
import { QuoteMysqlRepository } from '@quotes/infra/quote-mysq.repository';
import { QuoteMySqlRepositoryModule } from './quote-mysql.repository.module';
import { AirportRepositoryModule } from '@airports/main/airport.repository.module';
import { FindQuoteByIdController } from '@quotes/presentation/find-quote-by-id.controller';
import { FindQuoteByIdUseCase } from '@quotes/application/usecases/find-quote-by-id.usecase';
import { FindAllQuotesUseCase } from '@quotes/application/usecases/find-all-quotes.usecase';
import { FindAllQuotesController } from '@quotes/presentation/find-all-quotes.controller';
import { TripQuoteController } from '@quotes/presentation/trip-quote.controller';
import { TripQuoteUseCase } from '@quotes/application/usecases/tripe-quote.usecase';

@Module({
  controllers: [
    CreateQuoteController,
    FindQuoteByIdController,
    FindAllQuotesController,
    TripQuoteController,
  ],
  providers: [
    CreateQuoteUseCase,
    QuoteMysqlRepository,
    FindQuoteByIdUseCase,
    FindAllQuotesUseCase,
    TripQuoteUseCase,
  ],
  imports: [QuoteMySqlRepositoryModule, AirportRepositoryModule],
})
export class QuotesModule {}
