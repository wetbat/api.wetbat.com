import { Controller, Post, Body } from '@nestjs/common';
import { CreateQuoteUseCase } from '@quotes/application/usecases/create-quote.usecase';
import { ApiTags, ApiCreatedResponse } from '@nestjs/swagger';
import { CreateQuoteDto } from '@swagger/create-quote.dto';

@ApiTags('quotes')
@Controller('quotes')
export class CreateQuoteController {
  constructor(private readonly createQuoteUseCase: CreateQuoteUseCase) {}

  @Post()
  @ApiCreatedResponse({})
  create(@Body() createQuoteDto: CreateQuoteDto) {
    return this.createQuoteUseCase.exec(createQuoteDto);
  }
}
