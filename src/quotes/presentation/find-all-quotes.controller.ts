import { Controller, Get } from '@nestjs/common';
import { FindAllQuotesUseCase } from '@quotes/application/usecases/find-all-quotes.usecase';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { FindAllQuotesDto } from '@swagger/find-all-quotes.dto';

@ApiTags('quotes')
@Controller('quotes')
export class FindAllQuotesController {
  constructor(private readonly findAllQuotesUseCase: FindAllQuotesUseCase) {}

  @Get()
  @ApiOkResponse({ type: FindAllQuotesDto })
  findAll(): Promise<FindAllQuotesDto> {
    return this.findAllQuotesUseCase.exec();
  }
}
