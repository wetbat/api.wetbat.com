import { Controller, Get, Param } from '@nestjs/common';
import { TripQuoteUseCase } from '@quotes/application/usecases/tripe-quote.usecase';
import { ApiTags, ApiCreatedResponse } from '@nestjs/swagger';
import { QuotePriceDto } from '@swagger/quote-price.dto';

@ApiTags('quotes')
@Controller('quotes')
export class TripQuoteController {
  constructor(private readonly tripQuoteUseCase: TripQuoteUseCase) {}

  @Get('price/:id')
  @ApiCreatedResponse({ type: QuotePriceDto })
  tripQuote(@Param('id') id: string) {
    return this.tripQuoteUseCase.exec({ quoteId: id });
  }
}
