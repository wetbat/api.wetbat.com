import { Controller, Get, Param } from '@nestjs/common';
import { FindQuoteByIdUseCase } from '@quotes/application/usecases/find-quote-by-id.usecase';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { QuoteDto } from '@swagger/quote.dto';

@ApiTags('quotes')
@Controller('quotes')
export class FindQuoteByIdController {
  constructor(private readonly findQuoteByIdUseCase: FindQuoteByIdUseCase) {}

  @Get(':id')
  @ApiOkResponse({ type: QuoteDto })
  findById(@Param('id') id: string): Promise<QuoteDto> {
    return this.findQuoteByIdUseCase.exec({ id });
  }
}
