export abstract class DomainError extends Error {
  constructor(
    public readonly code: number,
    public readonly description: string,
  ) {
    super(description);
  }
}
