import { v4 as uuid } from 'uuid';

export abstract class Entity {
  private readonly id: string;
  constructor(id?: string) {
    this.id = id ?? uuid();
  }

  public getId(): string {
    return this.id;
  }

  public equal(entity: Entity): boolean {
    return this.id === entity.getId();
  }
}
