import { ValueObject } from './valueobject';
import { Entity } from './entity';

export interface Repository<T extends Entity | ValueObject> {}
