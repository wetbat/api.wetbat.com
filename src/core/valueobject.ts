export abstract class ValueObject {
  abstract equal(vo: ValueObject): boolean;
}
