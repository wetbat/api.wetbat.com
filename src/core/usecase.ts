import { Dto } from '@core/dto';

export interface UseCase<T extends Dto, U extends Dto> {
  exec(
    input: UseCase.InputDto<T>,
  ): Promise<UseCase.OutputDto<U>> | UseCase.OutputDto<U>;
}

export namespace UseCase {
  export type InputDto<T extends Dto> = T;
  export type OutputDto<T extends Dto> = T;
}
