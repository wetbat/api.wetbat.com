const config = require('./jest.config');
config.testMatch = ['**/*.e2e.(spec|test).ts'];
module.exports = config;