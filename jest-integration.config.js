const config = require('./jest.config');
config.testMatch = ['**/*.integration.(spec|test).ts'];
module.exports = config;
