FROM node:18.16.0

WORKDIR /var/www/app

COPY . .

# Instala as dependências do projeto
RUN npm i -g @nestjs/cli
RUN npm install
RUN npm run build

CMD ["npm", "run", "start:prod"]
