module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  collectCoverageFrom: ['**/*.ts', '!**/tests/*'],
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
  moduleNameMapper: {
    '@core/(.*)': '<rootDir>/core/$1',
    '@quotes/(.*)': '<rootDir>/quotes/$1',
    '@airports/(.*)': '<rootDir>/airports/$1',
    '@services/(.*)': '<rootDir>/services/$1',
    '@invoices/(.*)': '<rootDir>/invoices/$1',
  },
  modulePathIgnorePatterns: ['../<rootDir>/dist/'],
};
